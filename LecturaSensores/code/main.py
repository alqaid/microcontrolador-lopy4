
#       =====================================================
#       =========    main.py (1)             ================
#      =====================================================

import pycom
import time
import _thread  

from pysense import Pysense
from SI7006A20 import SI7006A20
from LTR329ALS01 import LTR329ALS01
from MPL3115A2 import MPL3115A2,ALTITUDE,PRESSURE
from LIS2HH12 import LIS2HH12

# ----- -----  Variables  ----- ----- 
_DELAY = 2 
_DELAY2 = 3
# ----- -----  INICIALIZAR   ----- ----- 
py = Pysense()    # OBJETO A PLACA PYSENS
si = SI7006A20(py)  # SENSOR DE TEMPERATURA y Humedad
li = LTR329ALS01(py)  # SENSOR DE LUMINOSIDAD
mplA = MPL3115A2(py, mode=ALTITUDE) # SENSOR DE ALTITUDE
mplP = MPL3115A2(py, mode=PRESSURE) # SENSOR DE PRESION
acc = LIS2HH12(py)

time1 = time
time2 = time
# ----- -----  FUNCIONES  ----- ----- 
def fun_LeerTemperatura(delay,id):
       while True:
              time1.sleep(delay)
              temperatura= si.temperature()
              print(id + " Temperatura: " + str(temperatura))             

def fun_LeerHumedad(delay,id):
       while True:
              time1.sleep(delay)
              Humedad= si.humidity()
              print(id + " Humedad: " + str(Humedad))    

def fun_LeerAltitud(delay,id):
       while True:
              time1.sleep(delay)
              Altitud= mplA.altitude()
              print(id + " Altitud: " + str(Altitud))    

def fun_LeerPresion(delay,id):
       while True:
              time1.sleep(delay)
              Presion= mplP.pressure()
              print(id + " Presion: " + str(Presion))    

def fun_LeerAccelerometro(delay,id):
       while True:
              time1.sleep(delay)
              vPitch= acc.pitch()
              vRoll= acc.roll()
              accel= acc.acceleration()
              print(id + " Pitch: " + str(vPitch) + "  - Roll: " + str(vRoll))
              print(id + " Acelormetro: x:%0.2f, y:%0.2f, z:%0.2f," % (accel[0],accel[1],accel[2]))  


def fun_LeerLuminosidad(delay,id):
       while True:
              time2.sleep(delay)
              luminosidad= li.light()
              
              print(id + " Luminosidad AZUL: %0.2f, ROJO: %0.2f" % (luminosidad[0],luminosidad[1]))
              if (luminosidad[0] < 50) and (luminosidad[1] < 50):
                       print("¡Luminosidad BAJA!")                       
                       pycom.heartbeat(False)
                       pycom.rgbled(0xff0000)                      
              else:
                     pycom.heartbeat(False) # VUELVO A OFF EL LED si se encendió
  

# ----- -----  CUERPO DEL PROGRAMA   ----- -----

pycom.heartbeat(False)  #Apagar el led al principio
 
_thread.start_new_thread(fun_LeerLuminosidad,(13,"h1"))
_thread.start_new_thread(fun_LeerTemperatura,(15,"h2"))
_thread.start_new_thread(fun_LeerHumedad,(17,"h3"))
_thread.start_new_thread(fun_LeerPresion,(21,"h4"))
_thread.start_new_thread(fun_LeerAltitud,(23,"h5"))
_thread.start_new_thread(fun_LeerAccelerometro,(2,"h6"))
