# Leer sensores con LoPy&copy; & Pysense&copy; & MQTT
Programa en Python para leer los sensores de la placa Pysense con el controlador LOPY4. El programa lanza 6 Threats (Hilos) cada uno con un temporizador, para ir devolviendo indefinidamente, por consola los valores de los sensores de Pysense: Luminosidad, Temperatura, Humedad, Presión, Altitud y Acelerometro (pitch, roll , x, y, z).
Peculiaridad: Si la luminosidad baja de un rango (50) el led se enciende en color rojo.


Para ello usamos las librerías:  LIS2HH12, LTR329ALS01, MPL3115A2, SI7006A20, incorporadas en directorio lib.



## Estructura del proyecto LECTURA DE SENSORES
El proyecto tiene la siguiente estructura:

-**README.md**(este fichero): describe el proyecto en sí.

-**LecturaSensores/pymakr.conf**: fichero de configuración para el plug-in Pymakr de Atom o Visual Code.

-**LecturaSensores/code/**: Contiene los ficheros necesarios para la aplicación a desarrollar para el microcontrolador LoPy&copy;

  -**LecturaSensores/code/lib/**: Directorio con todas librerias y módulos en MicroPython que pueden ser necesarios importar en nuestro programa.

  -**LecturaSensores/code/boot.py**: Programa ejecutado en primer lugar por el micro-controlador LoPy&copy; cuando arranca. Básicamente configura los parámetros de comunicación por el puerto serie y llama a la ejecución del programa main.py

  -**LecturaSensores/code/main.py**: Fichero con todo el código fuente.


## Puesta en marcha
Se utilizará el editor Visual Code o Atom, en ambos es compatible la librería Pymakr. 

En el caso de Visual Code la podemos instalar directamente desde su instalador de plugins buscándola como Pymakr.

Una vez instalado Pymakr y desde el raiz del proyecto, conectamos Lopy4 por usb y nos lo reconocerá inmediatemente.

-**NOTA**: en el archivo pymakr.conf la primera linea: "address": "COM4" sustituir COM4 por el puerto en el que detecte la librería pymakr al controlador LOPY4.

En visual Code aparecerá una barra de estado Pymakr en el pie del editor, con las comandos de Pymakr para ejecutar y cargar el código en Lopy4.

Mediante el comando RUN podemos lanzar el código a Lopy4.
Mediente el comando Upload lanzamos el código a la memoria flash de Lopy4.


Para parar un proyecto en marcha:

-**CTRL-C**: Stop any running code

-**CTRL-D**: soft reset
 


## proyecto ProtocoloMQTT
1º Conexión Wifi.
2º Conexión MQTT
2.1 Publicación y Suscripción de mensajes
2.1.a publica temperatura y luminosidad
2.1.b suscribe al estado del led on off. Modo automático o Manual de control de led, y Color del led.
3.1 Control del led, modo manual y modo automático. Esto se realizará desde NODE-RED u Otro servicio MQTT


El proyecto tiene la siguiente estructura:

-**README.md**(este fichero): describe el proyecto en sí.

-**protocoloMQTT/pymakr.conf**: fichero de configuración para el plug-in Pymakr de Atom o Visual Code.

-**protocoloMQTT/code/**: Contiene los ficheros necesarios para la aplicación a desarrollar para el microcontrolador LoPy&copy;

  -**protocoloMQTT/code/lib/**: Directorio con todas librerias y módulos en MicroPython que pueden ser necesarios importar en nuestro programa.

  -**protocoloMQTT/code/boot.py**: Programa ejecutado en primer lugar por el micro-controlador LoPy&copy; cuando arranca. Básicamente configura los parámetros de comunicación por el puerto serie y llama a la ejecución del programa main.py

  -**protocoloMQTT/code/main.py**: Fichero con todo el código fuente.


## Referencias:

Documentación Lopy4: https://docs.pycom.io/

Pymakr para Visual Code: https://marketplace.visualstudio.com/items?itemName=pycom.Pymakr

Documentación Python: https://www.w3schools.com/python/

## Autor:
Angel Alcaide: https://www.alcaide.info/lopy4
